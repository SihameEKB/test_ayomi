"""Web_App URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""

from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls import url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.contrib.auth.views import LogoutView
from . import views

urlpatterns = [
    
    url('home/', views.home, name='home'),
    url('register/', views.registerPage, name="register"),
	url('login/', views.loginPage, name="login"),  
	url('logout/',LogoutView.as_view(next_page=settings.LOGIN_REDIRECT_URL), name="logout"),
    url('profile/', views.profile, name="profile"),
    url('updateUser/', views.updateUser, name='updateUser'),
]

app_name='Web_App'