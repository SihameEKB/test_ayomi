from django.shortcuts import render, redirect, HttpResponseRedirect
from django.http import HttpResponse
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login, logout

from django.contrib import messages

from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_protect

from bootstrap_modal_forms.generic import BSModalCreateView


# Create your views here.
from .models import *
from .forms import CreateUserForm

# Register view
@csrf_protect
def registerPage(request):
	if request.user.is_authenticated:
		return redirect('/home')
	else:
		form = CreateUserForm()
		if request.method == 'POST':
			form = CreateUserForm(request.POST)
			if form.is_valid():
				form.save()
				user = form.cleaned_data.get('username')
				firstName = form.cleaned_data.get('first_name')
				lastName = form.cleaned_data.get('last_name')
				email = form.cleaned_data.get('email')
				messages.success(request, 'Account was created for ' + firstName + ' ' + lastName)

				return redirect('/login')
			

		context = {'form':form}
		return render(request, 'Web_App/register.html', context)

# Login view
@csrf_protect
def loginPage(request):
	if request.user.is_authenticated:
		return redirect('/profile')
	else:
		if request.method == 'POST':
			username = request.POST.get('username')
			firstName = request.POST.get('first_name')
			lastName = request.POST.get('last_name')
			password =request.POST.get('password')
			email =request.POST.get('email')

			user = authenticate(request, username=username, password=password)

			if user is not None:
				login(request, user)
				return redirect('/profile')
			else:
				messages.info(request, 'Username OR password is incorrect')

		context = {}
		return render(request, 'Web_App/login.html', context)

# Logout view 
@login_required(login_url='login')
def logoutUser(request):
		logout(request)
		return redirect('/login')

# Home view
@login_required(login_url='login')
def home(request):
	context = {}
	return render(request, 'Web_App/home.html', context)

# Profile view
@login_required(login_url='login')
def profile(request):
	context = {}
	return render(request, 'Web_App/profile.html', context)

# Update user view
@login_required(login_url='login')
def updateUser(request, pk):

	# user=User.objects.get(pk=1)
	# form = CreateUserForm(instance=user)

	# if request.method == 'POST':
	# 	form = CreateUserForm(request.POST, instance=user)
	# 	if form.is_valid():
	# 		form.save()
	# 		return redirect('/profile')

	context = {'form':form}
	return render(request, 'Web_App/updateUser.html', context)
